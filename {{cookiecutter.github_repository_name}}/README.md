# cookiecutter-drf-template


## Highlights

- Modern Python development with Python 3.9+
- Bleeding edge Django 3.1+
- PostgreSQL 13.1+


## Quick Start
(TBD create istruction for start project on local and docker-compose for test)

Install [cookiecutter](https://github.com/audreyr/cookiecutter):

```bash
pip install cookiecutter jinja2-git
```

Scaffold your project:
```bash
cookiecutter gl:itokar98/cookiecutter-drf-template
```

## License

MIT. See [LICENSE]() for more details.
