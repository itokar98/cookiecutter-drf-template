#!/usr/bin/env sh
set -o errexit
set -o nounset

# checker stust postgres
sh ./docker/wait-for-it.sh db:5432
# create table db
python /code/manage.py migrate --noinput
# start dev server
/usr/local/bin/gunicorn app.wsgi \
  --workers=4 `# Sync worker settings` \
  --max-requests=2000 \
  --max-requests-jitter=400 \
  --bind='0.0.0.0:8000' `# Run Django on 8000 port` \
  --chdir '/code'
  --log-file=- \
  --worker-tmp-dir='/dev/shm'
